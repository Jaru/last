require 'test_helper'

class OrderNotifierTest < ActionMailer::TestCase
  test "received" do
    mail = OrderNotifier.received(orders(:one))
    assert_equal "Store Order confim", mail.subject
    assert_equal ["janis@example.org"], mail.to
    assert_equal ["depot@example.com"], mail.from
    assert_match "1 x ziemas cepure", mail.body.encoded
  end

  test "shipped" do
    mail = OrderNotifier.shipped
    assert_equal "Store Order Shipped", mail.subject
    assert_equal ["janis@example.org"], mail.to
    assert_equal ["depot@example.com"], mail.from
    assert_match "1 x ziemas cepure", mail.body.encoded
  end

end
