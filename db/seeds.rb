# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# 1.
Product.create!(title: 'Cimdi', description: %{ <p> Ziemas cimdi </p> }, image_url: 'cimdi.png', price: 20.32)
Product.create!(title: 'Cepure', description: %{ <p> Ziemas Cepure </p> }, image_url: 'Cepure.png', price: 42.62)
Product.create!(title: 'Krekls', description: %{ <p> Ziemas Krekls </p> }, image_url: 'Krekls.png', price: 32.52)
Product.create!(title: 'Galds', description: %{ <p> Ziemas Galds </p> }, image_url: 'Galds.png', price: 21.42)
Product.create!(title: 'Monitors', description: %{ <p> Ziemas Monitors </p> }, image_url: 'Monitors.png', price: 25.23)
